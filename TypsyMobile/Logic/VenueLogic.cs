﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TypsyMobile.Model;

namespace TypsyMobile.Logic
{
    public class VenueLogic
    {

        public async static Task<List<Venue>> GetVenues(double latitude, double longitude)
        {
            List<Venue> venues = new List<Venue>();

            var url = VenueRoot.GenerateURL(latitude, longitude);

            using (HttpClient client = new HttpClient())
            {
                var response = await client.GetAsync(url);
                var json = await response.Content.ReadAsStringAsync();

                var venueRoot = JsonConvert.DeserializeObject<VenueRoot>(json);

                foreach (var item in venueRoot.response.groups[0].items)
                {
                    venues.Add(item.venue as Venue);
                }

            };

            return venues;

        }
    }

}
