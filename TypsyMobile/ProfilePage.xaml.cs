﻿using System;
using System.Collections.Generic;
using System.Linq;
using SQLite;
using TypsyMobile.Model;
using Xamarin.Forms;

namespace TypsyMobile
{
    public partial class ProfilePage : ContentPage
    {
        public ProfilePage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            using (SQLiteConnection conn = new SQLiteConnection(App.DatabasePath))
            {
                var locationTable = conn.Table<Locations>().ToList();

                // select disctinct categories from Location table 
                var categories = (from l in locationTable
                                orderby l.CategoryId
                                select l.CategoryName).Distinct().ToList();


                Dictionary<string, int> categoriesCount = new Dictionary<string, int>();
                foreach(var category in categories)
                {
                    var count = locationTable.Where(l => l.CategoryName == category).ToList().Count;

                    categoriesCount.Add(category, count);
                }

                categoryListView.ItemsSource = categoriesCount;

                locationCount.Text = locationTable.Count.ToString();

            }
        }
    }
}
