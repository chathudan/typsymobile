﻿using System;
using System.Collections.Generic;
using System.Linq;
using Plugin.Geolocator;
using SQLite;
using TypsyMobile.Logic;
using TypsyMobile.Model;
using Xamarin.Forms;

namespace TypsyMobile
{
    public partial class NewLocationPage : ContentPage
    {
        public NewLocationPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            var locator = CrossGeolocator.Current;
            var position = await locator.GetPositionAsync();

            var venues = await VenueLogic.GetVenues(position.Latitude, position.Longitude);

            venueListView.ItemsSource = venues;
        }

        void ToolbarItem_Save_Clicked(System.Object sender, System.EventArgs e)
        {
            try
            {
                var selectedLocation = venueListView.SelectedItem as Venue;
                var firstCategory = selectedLocation.categories.FirstOrDefault();

                Locations loc = new Locations()
                {
                    Place = locationEntry.Text,
                    CategoryId = firstCategory.id,
                    CategoryName = firstCategory.name,
                    Address = selectedLocation.location.address,
                    Distance = selectedLocation.location.distance,
                    Latitude = selectedLocation.location.lat,
                    Logitude = selectedLocation.location.lng,
                    Locationname = selectedLocation.name
                };

                using (SQLiteConnection conn = new SQLiteConnection(App.DatabasePath))
                {
                    conn.CreateTable<Locations>();
                    var rows = conn.Insert(loc);

                    if (rows > 0)
                        DisplayAlert("Success", "Lacation succesfully insert", "Ok");
                    else
                        DisplayAlert("Failure", "Lacation failed to be inserted", "Ok");
                }
            }catch(Exception ex)
            {
               
            }
        }
    }
}
