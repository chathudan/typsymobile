﻿using System;
using System.Collections.Generic;
using SQLite;
using TypsyMobile.Model;
using Xamarin.Forms;

namespace TypsyMobile
{
    public partial class HistoryPage : ContentPage
    {
        public HistoryPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            using (SQLiteConnection conn = new SQLiteConnection(App.DatabasePath))
            {
                conn.CreateTable<Locations>();
                var locations = conn.Table<Locations>().ToList();

                locationListView.ItemsSource = locations;
            }
        }

        void Handle_ItemSelected(System.Object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            var selectedLocation = locationListView.SelectedItem as Locations;

            if(selectedLocation != null)
            {
                Navigation.PushAsync(new LocationDetailPage(selectedLocation));
            }
        }
    }
}
