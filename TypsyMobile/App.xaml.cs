﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TypsyMobile
{
    public partial class App : Application
    {
        public static string DatabasePath = string.Empty;
        public App()
        {
            InitializeComponent();
            MainPage = new NavigationPage(new MainPage());
        }

        public App(string databasePath)
        {
            DatabasePath = databasePath;
            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
