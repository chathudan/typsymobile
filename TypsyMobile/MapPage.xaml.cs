﻿using System;
using System.Collections.Generic;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using SQLite;
using TypsyMobile.Model;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TypsyMobile
{
    public partial class MapPage : ContentPage
    {
		private bool hasLocationPermission = false;

        public MapPage()
        {
            InitializeComponent();

            GetPermissions();
        }

		/*
		 * Check and request location permission 
		 * */
        private async void GetPermissions()
        {
			try
			{
				var status = await CrossPermissions.Current.CheckPermissionStatusAsync<LocationPermission>();
				if (status != PermissionStatus.Granted)
				{
					if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Location))
					{
						await DisplayAlert("Need location", "Typsy need that location", "OK");
					}

					status = await CrossPermissions.Current.RequestPermissionAsync<LocationPermission>();
				}

				if (status == PermissionStatus.Granted)
				{
					hasLocationPermission = true; 
					locationMap.IsShowingUser = true;

					GetLocation();
				}
				else if (status != PermissionStatus.Unknown)
				{
					await DisplayAlert("Location denied","Location permission required...", "OK");
				}
			}
			catch (Exception ex)
			{
                await DisplayAlert("Error", ex.Message, "OK");
			}
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();

			if (hasLocationPermission)
			{
				var locator = CrossGeolocator.Current;

				locator.PositionChanged += Locator_PositionChanged;
				locator.StartListeningAsync(TimeSpan.Zero, 100);
			}
				GetLocation();

			using (SQLiteConnection conn = new SQLiteConnection(App.DatabasePath))
			{
				conn.CreateTable<Locations>();
				var locations = conn.Table<Locations>().ToList();

				DisplayInMap(locations);
			}
		}

		/*
		 * Display visited location in map 
		 * @see Location 
		 * **/
        private void DisplayInMap(List<Locations> locations)
        {
           foreach(var loc in locations)
            {
				try
				{
					var potiotion = new Xamarin.Forms.Maps.Position(loc.Latitude, loc.Logitude);
					var pin = new Xamarin.Forms.Maps.Pin()
					{
						Type = Xamarin.Forms.Maps.PinType.SavedPin,
						Position = potiotion,
						Label = loc.Locationname,
						Address = loc.Address

					};

					locationMap.Pins.Add(pin);
				}
				catch (NullReferenceException nre) { }
				catch (Exception ex) { }
				
            }
        }

        /*
		 * Stop location lisnig and and updating position changed 
		 * 
		 * **/
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
			CrossGeolocator.Current.StopListeningAsync();
			CrossGeolocator.Current.PositionChanged -= Locator_PositionChanged;

		}

        /*
		 * Location chencge listner update 
		 * **/
        private void Locator_PositionChanged(object sender, Plugin.Geolocator.Abstractions.PositionEventArgs e)
        {
			MoveMap(e.Position);
        }

        private async void GetLocation()
        {
			if (hasLocationPermission)
			{
				var locator = CrossGeolocator.Current;
				var position = await locator.GetPositionAsync();

				MoveMap(position);
			}
		}

        private void MoveMap(Position position) {

			var center = new Xamarin.Forms.Maps.Position(position.Latitude, position.Longitude);
			var span = new Xamarin.Forms.Maps.MapSpan(center, 1, 1);

			locationMap.MoveToRegion(span);
		}
    }

   
}
