﻿using System;
using SQLite;

namespace TypsyMobile.Model
{
    public class Locations
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [MaxLength(250)]
        public string Place { get; set; }

        public string Locationname { get; set; }

        public string CategoryId { get; set; }

        public string CategoryName { get; set; }

        public string Address { get; set; }

        public double Latitude { get; set; }

        public double Logitude { get; set; }

        public int Distance { get; set; }
    }
}
