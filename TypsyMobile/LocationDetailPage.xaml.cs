﻿using System;
using System.Collections.Generic;
using SQLite;
using TypsyMobile.Model;
using Xamarin.Forms;

namespace TypsyMobile
{
    public partial class LocationDetailPage : ContentPage
    {
        Locations selectedLocation; 

        public LocationDetailPage(Model.Locations selectedLocation)
        {
            InitializeComponent();

            this.selectedLocation = selectedLocation;

            locationEntry.Text = this.selectedLocation.Place;
        }

        void updateButton_Clicked(System.Object sender, System.EventArgs e)
        {
            selectedLocation.Place = locationEntry.Text;

            using (SQLiteConnection conn = new SQLiteConnection(App.DatabasePath))
            {
                conn.CreateTable<Locations>();
                var rows = conn.Update(selectedLocation);

                if (rows > 0)
                    DisplayAlert("Update", "Lacation succesfully updated", "Ok");
                else
                    DisplayAlert("Failure", "Lacation failed to be updated", "Ok");
            }

        }

        void deleteButton_Clicked(System.Object sender, System.EventArgs e)
        {
            using (SQLiteConnection conn = new SQLiteConnection(App.DatabasePath))
            {
                conn.CreateTable<Locations>();
                var rows = conn.Delete(selectedLocation);

                if (rows > 0)
                    DisplayAlert("Delete", "Lacation succesfully deleted", "Ok");
                else
                    DisplayAlert("Failure", "Lacation failed to be deleted", "Ok");
            }
        }
    }
}
