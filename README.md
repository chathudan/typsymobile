# TypsyMobile

I have implemented the below functionalities on both Android and iOS in this Xamarine project.

1. Retrieved nearby places from [Foresqure place API](https://developer.foursquare.com/docs/api-reference/venues/explore).
2. Displayed places on a list view.
3. Saved selected places in local storage using SQLite.
4. Google Map and Apple map integration.
5. Implemented location services and added location permission requests.
6. Displayed saved/visited places on relevant Maps with the marker.
7. Add/Edit and Delete places.
8. Profile view to show the total number of visited places and displayed distinct places types with number of visits.





